#!/usr/bin/env python3
#
# Copyright (C) 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
# Modifications copyright (C) 2023 CERN for the benefit of the ATLAS collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
# This code is an adaption of some of the testing routines found in 
# https://gitlab.com/dlr-dw/py-merlin/-/tree/main/tests to validate
# that the outputs of our versions of DRAG and MERLIN are equal to 
# the respective outpus of py-merlin.
# test_drag_topk is based on test_it_exposes_a_function_endpoint() found in
#
# https://gitlab.com/dlr-dw/py-merlin/-/blob/main/tests/test_discord_discovery_gemm3.py
#
# test_merlin_top_1 is based on test_it_returns_a_set_of_discords() found in
#
# https://gitlab.com/dlr-dw/py-merlin/-/blob/main/tests/test_merlin.py


import numpy as np
import numpy.typing as npt
import pytest

from .merlin_topk import merlin_topk, drag


def get_univariate_time_series() -> npt.NDArray:
    time_series_t = np.linspace(1, 1000, 1000)
    time_series_t = np.sin(time_series_t)
    for i in range(10):
        time_series_t[600 + i] = i
        time_series_t[609 + i] = 10 - i

    return time_series_t


class TestMerlin:
    def test_drag_topk(self):
        # given
        time_series_t = get_univariate_time_series()
        subseqlen = 15
        r = 3.8

        expected_discords = list(range(591, 615))
        expected_distances = [3.8121,
                                4.4826,
                                5.0789,
                                5.4300,
                                5.2033,
                                5.0381,
                                4.9255,
                                4.8308,
                                4.7058,
                                4.5918,
                                4.5890,
                                4.6749,
                                4.6749,
                                4.5890,
                                4.5766,
                                4.6638,
                                4.7847,
                                4.8276,
                                4.8271,
                                4.9100,
                                5.1464,
                                5.3740,
                                4.8819,
                                4.040]

        # As our implementation of DRAG only returns the maximum
        # discord, we have to select the discord and distance at
        # the index of maximum distance for validation.
        # We intentionally leave the original arrays from py-merlin
        # here instead of hardcoding the maximum discord and
        # distance to ensure easier comparability

        index_dist_max = np.argmax(expected_distances)

        # when
        nn_dist, discords = drag(time_series_t, subseqlen, r, [])

        # then

        np.testing.assert_equal(expected_discords[index_dist_max], discords)
        np.testing.assert_array_almost_equal(expected_distances[index_dist_max], nn_dist, decimal=4)


    def test_merlin_top_1(self):
        # given
        time_series_t = get_univariate_time_series()
        minl, maxl = 15, 25
        expected_idxs = [594, 612, 607, 601, 596, 612, 588, 603, 602, 595, 590]

        expected_distances = [5.43,
                                5.4433,
                                5.6304,
                                5.9793,
                                6.0839,
                                6.2652,
                                6.4118,
                                6.4835,
                                6.7384,
                                6.8088,
                                6.899]

        expected_lengths = list(range(15, 26))

        # when
        distances, discords, lengths = merlin_topk(time_series_t,
                                                            minl,
                                                            maxl,
                                                            1, False)

        discords = discords.flatten()
        distances = distances.flatten()
        lengths = lengths.flatten()

        # then
        assert len(discords) == 11
        assert len(distances) == 11
        assert len(lengths) == 11

        np.testing.assert_array_equal(expected_lengths, lengths)
        np.testing.assert_array_equal(expected_idxs, discords)
        np.testing.assert_array_almost_equal(expected_distances, distances, decimal=4)

    def test_it_verifies_input_dimensions(self):
        time_series_t = np.zeros((2, 5, 100))
        minl, maxl = 15, 25

        with pytest.raises(ValueError, match='Input time series '
                                                'is not one-dimensional'):
            # when
            _ = merlin_topk(time_series_t, minl, maxl, 1)


    # TODO: Implement test for top-K MERLIN
    def test_merlin_topk(self):
        pass