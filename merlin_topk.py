# Copyright (C) 2023 CERN for the benefit of the ATLAS collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# Python implementation of the top-K MERLIN algorithm described in
# "MERLIN++: parameter-free discovery of time series anomalies"
# by Nakamura et al..
# The code is derived from the MATLAB implementation published
# by the authors under
#
# https://drive.google.com/file/d/1JPtzUbvNUpVn52pJWAHrs28wEvp88TPP/view?usp=sharing
#


import math
import logging

import numpy as np
import bottleneck as bn
import pandas as pd
from tqdm import tqdm

    
def merlin_topk(time_series: np.ndarray,
                L_min: int,
                L_max: int,
                K: int,
                apply_near_constant_fix: bool=True,
                enable_progress_bar: bool=True):
    '''
    This function implements the top-K MERLIN algorithm as proposed in
    "MERLIN++: parameter-free discovery of time series anomalies"
    by Nakamura et al..

    Parameters
    ---------
    time_series : numpy.ndarray
        Input time series
    L_min : int
        Minimum subsequence length
    L_max : int
        Maximum subsequence length
    r : int
        Range parameter
    apply_near_constant_fix : bool
        Whether to apply the fix for the algorithm producing large discords
        in constant regions through addition of a large amplitude trend
        described by Nakamura et al.

    Returns
    -------
    distances : numpy.ndarray
        Array of shape (L_max-L_min+1, K) containing the top-K discords for each value of L
    indices : numpy.ndarray
        Array of shape (L_max-L_min+1, K) containing the indices of the top-K
        discords for each value of L
    lengths : numpy.array
        Array of shape (L_max-L_min+1,) containing the tested values of L
    '''

    # Ensure the input time series is a
    # floating-point numpy.ndarray
    
    time_series = np.asfarray(time_series)

    if time_series.ndim > 1:
        raise ValueError('Input time series is not one-dimensional')

    sum_nan = np.sum(np.isnan(time_series))
    if sum_nan > 0:
        raise ValueError(f'Time series contains {sum_nan}'
                            'NaN value(s). Please handle and retry.')
    
    # z-normalization
    time_series = (time_series - np.mean(time_series))/np.std(time_series)

    if apply_near_constant_fix == True:
        # Addition of constant trend
        time_series += np.arange(len(time_series))

    else:
        for i in range(0, len(time_series) - L_min + 1):
            if np.std(time_series[i:i + L_min]) == 0:
                raise RuntimeError('There is region so close '
                                    'to constant that the results '
                                    'will be unstable. Delete the '
                                    'constant region or try again '
                                    'with a L_min longer than the '
                                    'constant region.')


    length_count = L_max - L_min + 1

    distances = np.full((length_count, K), np.NINF)
    indices = np.zeros((length_count, K), dtype=np.int64)
    lengths = np.arange(L_min, L_max + 1, dtype=np.int64)
    
    exclusion_indices = []

    k_multiplier = 1

    r = 2*math.sqrt(L_min)

    with tqdm(total=K*length_count,
                desc='MERLIN Computation',
                disable=not enable_progress_bar) as pbar:
        for ki in range(0, K):
            while distances[0, ki] < 0:
                distances[0, ki], indices[0, ki] = drag(time_series,
                                                            lengths[0],
                                                            r*k_multiplier,
                                                            exclusion_indices)
                
                if ki == 0:
                    r = 0.5*r

                else:
                    k_multiplier = 0.95*k_multiplier

            exclusion_indices.append(indices[0, ki])
            pbar.update(1)
        
        for i in range(1, 5):
            if i >= length_count:
                return distances, indices, lengths

            exclusion_indices = []

            k_multiplier = 1

            r = 0.99*distances[i - 1, 0]
            
            for ki in range(K):

                while distances[i, ki] < 0:
                    distances[i, ki], indices[i, ki] = drag(time_series,
                                                                lengths[i],
                                                                r*k_multiplier,
                                                                exclusion_indices)

                    if ki == 0:
                        r = 0.99*r

                    else:
                        k_multiplier = 0.95*k_multiplier

                exclusion_indices.append(indices[i, ki])
                pbar.update(1)
        
        if length_count < 6:
            return distances, indices, lengths
        
        for i in range(5, length_count):

            exclusion_indices = []

            k_multiplier = 1

            m = np.mean(distances[i - 5:i], axis=0)[0]
            s = np.std(distances[i - 5:i], axis=0)[0]

            r = m - 2*s

            for ki in range(K):

                while distances[i, ki] < 0:
                    distances[i, ki] , indices[i, ki] = drag(time_series,
                                                                lengths[i],
                                                                r*k_multiplier,
                                                                exclusion_indices)

                    r=0.99*r

                    if ki == 0:
                        r=0.99*r

                    else:
                        k_multiplier = 0.95*k_multiplier

                exclusion_indices.append(indices[i, ki])
                pbar.update(1)

    return distances, indices, lengths


def drag(time_series: np.ndarray,
                        L: int,
                        r: float,
                        exclusion_indices: list):
    '''
    This implements discord discovery as described in
    "Disk Aware Discord Discovery: Finding Unusual Time Series in Terabyte Sized
    Datasets" by Yankovet al..

    Parameters
    ---------
    time_series : numpy.ndarray
        Input time series
    L : int
        Subsequence length
    r: int
        Range parameter
    exclusion_indices: list
        Exclusion indices

    Returns
    -------
    disc_dist : 
        Top discord distance. This value is -numpy.INF if the chosen value of r is too small
    disc_loc :
        Starting index of the top discord in the input time series.  This value is 0 if the
        chosen value of r is too small
    '''

    if not time_series.ndim == 1:
        raise RuntimeError('Expected a 1D input for time series')
    
    if (math.floor(len(time_series)/2) < L) or (L < 4):
        raise RuntimeError('Subsequence length parameter '
                            'must be in the range of '
                            '4 < L <= floor(len(TS)/2).')
    
    min_separation = L

    mu = bn.move_mean(time_series, L)[L-1:]
    sig = bn.move_std(time_series, L)[L-1:]

    subseq_count = len(time_series) - L + 1

    # Candidates Selection Phase

    C = pd.Series([(time_series[:L] - mu[0])/sig[0]], [0])

    for i in range(1, subseq_count):
        si = (time_series[i:i + L] - mu[i])/sig[i]

        cands = C.index

        is_cand = True

        for j in range(len(cands)):
            if abs(cands[j] - i) < min_separation:
                continue

            d = np.linalg.norm(si - C[cands[j]])

            if d < r:
                C.drop(cands[j], inplace=True)
                is_cand = False
                break

        if is_cand:  
            C[i] = si
    
    cands = C.index

    for ci in range(len(C.index)):
        for ei in range(len(exclusion_indices)):
            if np.abs(exclusion_indices[ei] - cands[ci]) <= L:
                C.drop(cands[ci], inplace=True)
                break

    if C.empty:
        logging.info(f'The r parameter of {r:.5f} was '
                        'too large for DRAG to work, '
                        'try making it smaller')
        
        disc_dist =- np.INF

        disc_loc = 0

        return disc_dist, disc_loc
    
    
    # Discord Refinement Phase

    cand_nn_dists_1 = pd.Series(np.full((len(C),), np.Inf), index=C.index)
    cand_nn_pos = pd.Series(np.full((len(C),), np.NaN), index=C.index)

    r_1 = r**2
    
    for i in range(subseq_count):

        if C.empty:
            break

        keys = C.index

        si = (time_series[i:i + L] - mu[i])/sig[i]

        for j in range(len(keys)):

            if abs(i - keys[j]) < min_separation:
                continue

            dist_1 = 0
            cand_idx = keys[j]
            cand_j = C[cand_idx]

            nn_dist_1 = cand_nn_dists_1[cand_idx]

            # We precompute the values here as
            # opposed to the MATLAB equivalent
            # as the numpy array functions are
            # much faster compared to calculating
            # the distances inside the loop

            dists_1 = np.square(si - cand_j)

            for k in range(L):
                dist_1 += dists_1[k]
                if dist_1 >= nn_dist_1:
                    break

            if dist_1 < r_1:
                C.drop(cand_idx, inplace=True)
                cand_nn_dists_1.drop(cand_idx, inplace=True)
                cand_nn_pos.drop(cand_idx, inplace=True)
                continue

            else:
                if dist_1 < cand_nn_dists_1[cand_idx]:
                    cand_nn_dists_1[cand_idx] = dist_1
                    cand_nn_pos[cand_idx] = i

    if C.empty:

        logging.debug(f'The r parameter of {r:.5f} was '
                            'too large for DRAG to work, '
                            'try making it smaller')

        disc_dist =- np.Inf
        disc_loc = 0

    else:

        # Search for top discord among candidates

        keys = cand_nn_dists_1.index

        # disc_dist_1 = 0
        # disc_loc = np.NaN

        # for i in range(len(cand_nn_dists_1)):

        #     if cand_nn_dists_1[keys[i]] > disc_dist_1:
        #         disc_dist_1 = cand_nn_dists_1[keys[i]]
        #         disc_loc = keys[i]

        disc_dist = np.sqrt(np.amax(cand_nn_dists_1))
        disc_loc = keys[np.argmax(cand_nn_dists_1)]

        disc_nnloc = cand_nn_pos[disc_loc]

        logging.info(f'The top discord of length {L} '
                        f'is at {disc_loc}, with a discord '
                        f'distance of {disc_dist:.2f}. Its nearest '
                        f'neighbor is at {disc_nnloc}')
            
    return disc_dist, disc_loc
    