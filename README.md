# MERLIN-topK

A simple Python implementation of the top-K MERLIN anomaly detection algorithm proposed by Nakamura et al. in [MERLIN++: parameter-free discovery of time series anomalies](https://link.springer.com/article/10.1007/s10618-022-00876-7). The code is derived from the MATLAB implementation by the authors of the paper provided [here](https://drive.google.com/file/d/1JPtzUbvNUpVn52pJWAHrs28wEvp88TPP/view?usp=sharing).

The test code for the top-1 case is derived from a different Python implementation of MERLIN provided by the DLR found [here](https://gitlab.com/dlr-dw/py-merlin/).

## Installation of Requirements

To install the Python requirements use

```shell
pip3 install -r requirements.txt

```

## Tests

After installation of the requirements, the tests can be run by executing `pytest` in the repository folder.
